<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CertificationBtn</name>
   <tag></tag>
   <elementGuidId>502d05ec-1da7-4472-96c3-2a07649de7d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@type='button' and contains(.,' Certificate')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@type='button' and contains(.,' Certificate')]</value>
   </webElementProperties>
</WebElementEntity>
