<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ConfirmBtn</name>
   <tag></tag>
   <elementGuidId>45328fde-d450-4473-bd95-7d8b280b10b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@type='button' and contains(.,'Confirm')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@type='button' and contains(.,'Confirm')]</value>
   </webElementProperties>
</WebElementEntity>
