<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>IssuerNameInputBox</name>
   <tag></tag>
   <elementGuidId>ad3f29c7-efe2-458e-b935-c37d6d54ac6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@id='certificate-issuer'])[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//form[@class='ng-invalid ng-untouched ng-pristine']//input[@id='certificate-issuer']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//form[@class='ng-invalid ng-untouched ng-pristine']//input[@id='certificate-issuer']</value>
   </webElementProperties>
</WebElementEntity>
