<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CodaGlobalAccountBtn</name>
   <tag></tag>
   <elementGuidId>e1395d99-d305-4d65-904d-ae0529bb64ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(text(),'CODA GLOBAL ACCOUNT')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;auth0-lock-container-1&quot;]/div/div[2]/form/div/div/div/div[2]/div[2]/span/div/div/div/div/div/div/div/div/button/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'CODA GLOBAL ACCOUNT')]</value>
   </webElementProperties>
</WebElementEntity>
