<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PeopleLink</name>
   <tag></tag>
   <elementGuidId>8f1d3f41-a097-4fb8-a221-58940e6775f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()='People']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()='People']</value>
   </webElementProperties>
</WebElementEntity>
