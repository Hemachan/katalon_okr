import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login'), [('Uname') : 'auth.poc@coda.global', ('Password') : 'nji9MKO)'], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Dashboard/AchievedMenu'))

WebUI.waitForElementPresent(findTestObject('Archieved/CertificationBtn'), 5)

WebUI.click(findTestObject('Archieved/CertificationBtn'))

int randomNumber

randomNumber = ((Math.random() * 500) as int)

WebUI.waitForElementPresent(findTestObject('Archieved/ConfirmBtn'), 5)

WebUI.setText(findTestObject('AddCertification/IssuerNameInputBox'), IssuerName)

WebUI.setText(findTestObject('AddCertification/CertificationNameInputBox'), CertificationName + randomNumber)

WebUI.click(findTestObject('AddCertification/IssedOnDatePicker'))

WebUI.click(findTestObject('AddCertification/IssueDateSelector'))

WebUI.click(findTestObject('AddCertification/ExpiresOnDatePicker'))

WebUI.click(findTestObject('AddCertification/ExpireDateLabel'))

WebUI.click(findTestObject('AddCertification/CertificationIDInputBox'))

WebUI.sendKeys(findTestObject('AddCertification/CertificationIDInputBox'), '4444')

WebUI.click(findTestObject('AddCertification/ConfirmButton'))

WebUI.verifyElementPresent(findTestObject('AddCertification/SuccessMessage'), 1)

