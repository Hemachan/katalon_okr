import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login'), [('Uname') : 'auth.poc@coda.global', ('Password') : 'nji9MKO)'], FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Objective/ObjectivesTab'), wait_time_long)

WebUI.click(findTestObject('Objective/ObjectivesTab'))

int i = 0

for (def obj : (0..1)) {
    WebUI.waitForElementPresent(findTestObject('Objective/AddButton'), wait_time )

    WebUI.click(findTestObject('Objective/AddButton'))

    WebUI.waitForElementPresent(findTestObject('Objective/ConfirmButton'), wait_time)

    WebUI.delay(sleep_time)

    curDate = CustomKeywords.'GeteCurrentDate.getcurrentdate'()

    System.out.println(curDate)

    WebUI.sendKeys(findTestObject('Objective/AddObjectiveTitleTextField'), objective_name + curDate)

    WebUI.click(findTestObject('Objective/ConfirmButton'))

    WebUI.waitForElementPresent(findTestObject('Objective/AddKeyResultButton'), wait_time)

    WebUI.click(findTestObject('Objective/AddKeyResultButton'), FailureHandling.STOP_ON_FAILURE)

    WebUI.waitForElementPresent(findTestObject('Objective/ConfirmKeyResultButton'), wait_time)

    WebUI.delay(sleep_time)

    WebUI.setText(findTestObject('Objective/AddToDoTitleTextField'), 'test KR')

    WebUI.click(findTestObject('Objective/ConfirmKeyResultButton'))

    WebUI.delay(sleep_time)

    i = (i + 1)

    if (i == 2) {
        WebUI.click(findTestObject('Objective/SettingIcon'))

        WebUI.waitForElementPresent(findTestObject('Objective/DeleteLink'), wait_time)

        WebUI.click(findTestObject('Objective/DeleteLink'))

        WebUI.waitForElementPresent(findTestObject('Objective/DeleteObjConfirmButton'), wait_time)

        WebUI.click(findTestObject('Objective/DeleteObjConfirmButton'))
    } else {
        WebUI.click(findTestObject('Objective/ObjectivesTab'))

        WebUI.delay(sleep_time)
    }
}

